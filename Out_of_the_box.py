# For Out of the Box I had to make several enhancements to Python tasks from the "Ok Level" and "Nice to have"
# To did this I should:
#1.Create candidates on redis-server,
#2.Send this candidate on rest-server,
#3.And verify that the candidate that was created on redis-server is the same as in rest-server.
#Test.1 Add a new candidates on rest-server from redis-server that accepts name and position,headers = {'content-type': 'application/json'} Response-201 -> positive test
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add the candidate that was added from redis-server to rest-server that accepts name, position and headers = {'content-type': 'application/json'}
#3. Verify that the new candidate was added on rest-server from redis-server
import redis # I must use this library to add a new candidate on redis-server
import requests
import json

pool = redis.ConnectionPool(host='localhost', port=6379, db=0) # connection with redis-server
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd: #1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidate_get = r_pool.get(id)
        return json._default_decoder.decode(candidate_get)

name = 'hot'
position = 'dog'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('1', name, position, r_pool)

class RequestAddPost:#2. Add the candidate that was added from redis-server to rest-server that accepts name, position and headers
    def postmethod(self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod(candidate_json)
#print x.data
#print x.our_request.status_code
#print candidate_json

if (x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    #print 'Test.1  Successful. A new candidate was added from redis-server to rest-server - Respond 201'
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']

    if(response_name==name and response_position==position):
        print 'Test.1 Successful. A new candidate was added from redis-server to rest-server and verified. Respond 201'
    elif(response_name != name or response_position != position):
        print 'Test.1 Failed. A new candidate did not add from redis-server to rest-server'
else:
    print 'Test.1 Failed. A new candidate did not add from redis-server to rest-server Response:' + str(x.our_request.status_code)

#__________________________________________________________________________________________________________________________________
#Test.2 Next test add a new candidate on rest-server from redis-server that accepts only position,Content-Type: application/json Response -400--->Positive
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add the candidate that was added from redis-server to rest-server that that accepts only position and headers = {'content-type': 'application/json'}
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd: #1. Add a new candidate on the redis-server
    def redisaddcandidateposition (self,id,position,r_pool):
        hm = json._default_encoder.encode({'position': position})
        r_pool.set(id, hm)
        candidate_get = r_pool.get(id)
        return json._default_decoder.decode(candidate_get)

position = 'try'

rd = RedisAdd()
candidate_json = rd.redisaddcandidateposition('2', position, r_pool)

class RequestAddPost:#2. Add the candidate that was added from redis-server to rest-server that that accepts only position and headers = {'content-type': 'application/json'}
	def postmethodEmptyName(self, candidate):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps(candidate)
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethodEmptyName(candidate_json)
#print x.data
#print x.our_request.status_code

if (x.our_request.status_code==400): #3. Verify that the new candidate was added on rest-server from redis-server
    print 'Test.2 Successful. Method POST works as expected when name is absent. Response: 400'
else:
    print 'Test.2 Failed. Method POST when name is absent works wrong. Response:' + str(x.our_request.status_code)

#___________________________________________________________________________________________________________________
#Test.3 Next test add a new candidate on rest-server from redis-server that accepts name and position in body without headers = {'content-type': 'application/json'} Response -400--->Positive
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add the candidate that was added from redis-server to rest-server that that accepts name and position in body without headers = {'content-type': 'application/json'}
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidateabsentheaders (self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidate_get = r_pool.get(id)
        return json._default_decoder.decode(candidate_get)

name = 'pop'
position = 'music'

rd = RedisAdd()
candidate_json = rd.redisaddcandidateabsentheaders('3', name, position, r_pool)

class RequestAddPost:#2. Add the candidate that was added from redis-server to rest-server that that accepts name and position in body without headers
    def postmetodemptyheader (self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        self.our_request = requests.post(url,self.data)

x = RequestAddPost()
x.postmetodemptyheader(candidate_json)
#print x.data
#print x.our_request.status_code
#print candidate_json

if (x.our_request.status_code==400):#3. Verify that the new candidate was added on rest-server from redis-server
    print 'Test.3 Successful. Method POST works as expected when headers is absent. Response: 400'
else:
    print 'Test.3 Failed. Method POST when headers is absent works wrong. Response:' + str(x.our_request.status_code)

#_______________________________________________________________________________________________________________________
#Test.4 Add similar candidates on rest-server from redis-server that accepts name and position, Content-Type: application/json --->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add similar candidates that were added from redis-server to rest-server that accepts name and position, Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = 'test'
position = 'QA'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('4', name, position, r_pool)
#print candidate_json
name = 'test'
position = 'QA'

rd = RedisAdd()
candidate_json1 = rd.redisaddcandidate('5', name, position, r_pool)
#print  candidate_json1

class RequestAddPost:#2. Add similar candidates that were added from redis-server to rest-server that accepts name and position, Content-Type: application/json
    def postmethod(self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod(candidate_json)
first_post_status_code = x.our_request.status_code
y = RequestAddPost()
y.postmethod(candidate_json1)
second_post_status_code = y.our_request.status_code

if(first_post_status_code==201 and second_post_status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    add_post_json1 = x.our_request.json()[u'candidate']
    response_id1 = add_post_json1[u'id']

    add_post_json2 = y.our_request.json()[u'candidate']
    response_id2 = add_post_json2[u'id']

    if(response_id1!=response_id2):
        print "Test.4 Two similar candidates were added on rest-server from redis-server and verified"
    else:
        print "Test.4 Two similar candidates were not added on rest-server from redis-server,verified"
else:
    print "Test.4 Two similar candidates were not added on rest-server from redis-server Response: " + str(x.our_request.status_code)

#______________________________________________________________________________________________________________________
#Test.5 Add a new candidate on rest-server from redis-server that accepts only name in body,Content-Type: application/json --->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server that accepts only name in body,Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,r_pool):
        hm = json._default_encoder.encode({'name':name})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = 'Tedy'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('6', name, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server that accepts only name in body,Content-Type: application/json
	def postmetodemptyPosition (self, candidate):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps(candidate)
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers = headers)
x = RequestAddPost()
x.postmetodemptyPosition(candidate_json)

if (x.our_request.status_code == 201):#3. Verify that the new candidate was added on rest-server from redis-server
    #print 'Test.5  A new candidate was added to rest-server from redis-server without position'
    add_post_json = x.our_request.json()[u'candidate']
    response_name = x.our_request.json()[u'candidate'][u'name']
    if (response_name == name):
        print 'Test.5 A new candidate was added to rest-server from redis-server without position and verified'
    else:
        print 'Test.5 A new candidate was not added to rest-server from redis-server without position, verified'
else:
    print 'Test.5 A new candidate was not added to rest-server from redis-server without position'


#_____________________________________________________________________________________________________________________
#Test.6 Next test will verify can I create a new candidate on rest-server from redis-server with two names and headers = Content-Type: application/json -->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server with two names and headers = Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server

pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,name2,r_pool):
        hm = json._default_encoder.encode({'name': name, 'name': name2})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = 'Tom'
name2 = 'Bob'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('7', name, name2, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server with two names and headers
    def postmethodchangeposition(self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethodchangeposition(candidate_json)

if(x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    # print 'Test.6 A new candidate was added to rest-server from redis-server with two names'
    response_name = x.our_request.json()[u'candidate'][u'name']

    if(response_name==name):
        print 'Test.6 A new candidate was added to rest-server from redis-server, name = name'
    elif(response_name==name2):
        print 'Test.6 A new candidate was added to rest-server from redis-server, name = name2'
else:
    print 'Test.6 A new candidate was not added to rest-server from redis-server with two names. Response:' + str(x.our_request.status_code)
#_______________________________________________________________________________________________________________________
#Test.7 Next test will verify can I add a new candidate on rest-server from redis-server with two position in body, headers = Content-Type: application/json--> Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server with two positions and headers = Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redis_add_candidate_absent_name(self,id,position,position2,r_pool):
        hm = json._default_encoder.encode({'position': position, 'position': position2})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

position = 'Actor'
position2 = 'QA'

rd = RedisAdd()
candidate_json = rd.redis_add_candidate_absent_name('8', position, position2, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server with two positions and headers = Content-Type: application/json
    def post_method_add_absent_name(self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.post_method_add_absent_name(candidate_json)

if(x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    #print 'Test.7 A new candidate was added to rest-server from redis-server with two positions'
    response_position = x.our_request.json()[u'candidate'][u'position']
    if(response_position==position):
        print 'Test.7 A new candidate was added to rest-server from redis-server with two positions, position = position'
    elif(response_position==position2):
        print 'Test.7 A new candidate was added to rest-server from redis-server with two positions, position = position2'
else:
    print 'Test.7 A new candidate was not added to rest-server from redis-server with two positions. Response:' + str(x.our_request.status_code)
#_______________________________________________________________________________________________________________________
#Test.8 Add a new candidate on rest-server from redis-server that accepts empty name and empty position in body,Content-Type: application/json. --->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server that accepts empty name and empty position in body, headers = Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = None
position = None

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('9', name, position, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server that accepts empty name and empty position in body, headers = Content-Type: application/json
    def postmethod(self, candidate):
        url = 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url, self.data, headers=headers)


x = RequestAddPost()
x.postmethod(candidate_json)

if (x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    #print 'Test.8 A new candidate that name and position - 'null' was added from redis-server to rest-server - Response 201'
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']

    if(response_name==name and response_position==position):
        print 'Test.8 A new candidate that name and position were empty was added from redis-server to rest-server and verified'
    elif(response_name != name or response_position != position):
        print 'Test.8 A new candidate that name and position were empty was not added from redis-server to rest-server, verified'
else:
    print 'Test.8 A new candidate that name and position - empty was not added from redis-server to rest-server. Response:' + str(x.our_request.status_code)

#_______________________________________________________________________________________________________________________
#Test.9 Add a new candidate on rest-server from redis-server that accepts 40 symbols in name and position in body,Content-Type: application/json, -->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server that accepts 40 symbols in name and position in body, headers = Content-Type: application/json
#3. Verify that the new candidate was added on rest-server from redis-server
pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate(self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = 'zooooooooozooooooooozooooooooozooooooooo'
position = 'coooooooolcoooooooolcoooooooolcooooooool'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('10', name, position, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server that accepts e40 symbols in name and position in body, headers
    def postmethod(self,candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod(candidate_json)

if (x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    #print 'Test.9 A new candidate that name and position has 40 symbols was added from redis-server to rest-server - Response 201'
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']

    if(response_name==name and response_position==position):
        print 'Test.9 A new candidate that name and position has 40 symbols was added from redis-server to rest-server and verified'
    elif(response_name != name or response_position != position):
        print 'Test.9 A new candidate that name and position has 40 symbols was not added from redis-server to rest-server, verified'
else:
    print 'Test.9 A new candidate that name and position has 40 symbols was not added from redis-server to rest-server. Response:' + str(x.our_request.status_code)
#_______________________________________________________________________________________________________________________
#Test.10 Add a new candidate on rest-server from redis-server that has wrong Content-Type:'application/x-www-form-urlencoded' -->Negative
#STEPS to do:
#1. Add a new candidate on the redis-server
#2. Add a new candidate on rest-server from redis-server that has wrong Content-Type:'application/x-www-form-urlencoded
#3. Verify that the new candidate was added on rest-server from redis-server

pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
r_pool = redis.Redis(connection_pool=pool)

class RedisAdd:#1. Add a new candidate on the redis-server
    def redisaddcandidate (self,id,name,position,r_pool):
        hm = json._default_encoder.encode({'name':name,'position': position})
        r_pool.set(id, hm)
        candidat_get = r_pool.get(id)
        return json._default_decoder.decode(candidat_get)

name = 'Alice'
position = 'Girl'

rd = RedisAdd()
candidate_json = rd.redisaddcandidate('11', name, position, r_pool)

class RequestAddPost:#2. Add a new candidate on rest-server from redis-server that has wrong Content-Type:'application/x-www-form-urlencoded
    def postmethodwrongheader(self, candidate):
        url= 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps(candidate)
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethodwrongheader(candidate_json)

if (x.our_request.status_code==201):#3. Verify that the new candidate was added on rest-server from redis-server
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']
    if(response_name==name and response_position==position):
        print 'Test.10 A new candidate that has wrong headers was added from redis-server to rest-server and verified'
    elif(response_name != name or response_position != position):
        print 'Test.10 A new candidate that has wrong headers was not added from redis-server to rest-server, verified'
else:
    print 'Test.10 A new candidate that has wrong headers was not added from redis-server to rest-server. Response:' + str(x.our_request.status_code)



