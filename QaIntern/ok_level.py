
#For Ok_Level I had to create Code that verifies that data is added to server, and also, Code that verifies correctness of returned response codes.
#So I started with download the library "requests" which helped me in creating requests.
#Also I used the library "json" which helped me work with json format of data.
#This is the Code that verifies that some data is added to server:
import json 
import requests
url= 'http://qainterview.cogniance.com/candidates'
headers = {'content-type': 'application/json'}
data = json.dumps({'name': 'Olga', 'position': 'QA intern'})
r = requests.post(url,data, headers=headers)


#And then Code that verifies correctness of returned response codes. 
#According to Api Documentation it must be 201:
if r.status_code != 201:
	print 'Failed! A new candidate was not added! POST method. The response code: ' + r.status_code
else:
	print 'Successfull! POST method: The response code - 201.'
r.content

#Then I had to verify that all methods on server work as expected. 
#That is why the next verification was POST method when header Content-Type: application/json is absent.
#Response code must be 400: 
r = requests.post(url,data)
if r.status_code != 400:
	print 'Failed! Wrong Response! POST method, when headers Content-Type is absent. The response code: ' + r.status_code
else:
	print 'Successfull! POST method, when headers Content-Type is absent: The response code - 400'

# And the last verification of POST method is when name is absent and response code is 400:
data = json.dumps({'position': 'position'})
r = requests.post(url, data, headers=headers)
if r.status_code != 400:
	print 'Failed! Wrong Response! POST method, when name is absent. The response code: ' + r.status_code
else:
	print 'Successfull! POST method, when name is absent. The response code - 400'
#The next method verification is GET that gives a list of all candidates and the response code is 200:
r = requests.get(url, headers=headers)
if r.status_code != 200:
	print 'Failed! Wrong Response! GET method. The response code: ' + r.status_code
else:
	print 'Successfull! GET method. A list of all candidates! The response code - 200'


#Also we must test GET method by id and get response code 200:
id = '1'
r = requests.get(url + '/' + str(id))
if r.status_code != 200:
	print 'Failed! Wrong Response! GET method by id. The response code: ' + r.status_code
else:
	print 'Successfull! GET method by id. The response code - 200, but method work not as expected'
#Verification of this method showed that not all method work not as expected and this method doesn't show data by id.

#For DELETE method get response code 200:
id = '1'
r = requests.delete(url + '/' + str(id))
if r.status_code != 200:
	print 'Failed! Wrong Response! DELETE method by id. The response code: ' + r.status_code
else:
	print 'Successfull! DELETE method by id. The response code - 200.'
	

	


