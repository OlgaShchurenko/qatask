Task for QA intern position include 3 levels. I made all of them. This is instruction how to revise it:
1. My first and MUST level is OK LEVEL. For this task I created Python file -> ok_level.py
This file can be run in command line to ensure that all code is working. In this task, I downloaded and used requests library for Python and used json library for Python.
If you open this file in Notepad ++ or JetBrains PyCharm (all this instrument I used to create my code) you can see my code with description of the methods which is included in API Documentation. 
2. My second  level is NICE TO HAVE.
This level is devided in two tasks:
2.1. To create positive and negative verifications using Python.
For this task I created Python file -> NICE_TO_HAVE.py
This file includes different positive and negative tests. You can also run it in command line to ensure that all code is working. 
And also open this file in Notepad ++ or JetBrains PyCharm where you can see all my tests with descriptions.
2.2. Use existing access log to count # of successful requests per hour.
The result of this task is in access_log.txt. You should open this file and see there how I get the result.
If you want to ensure that it works you should change in this file -> /home/o.schurenko/Documents/datamining.log , on your way where exists datamining.log  in your computer.
The access_log.txt include the results of tasks 2.2. for NICE TO HAVE and 3.2. OUT OF THE BOX
You should copy and run this code (2.2. for NICE TO HAVE)  from access_log.txt in unix command line.
3. The last level is OUT OF THE BOX. For this task I created Python file -> Out_of_the_box.py
This level also devided in two tasks:
3.1.For this task I created Python file -> Out_of_the_box.py
In this file there are a lot of tests where I created candidates in redis DB and then added this candidates on rest-server. All code with description you can see in Notepad ++ or JetBrains PyCharm or similar to them.
For this task I downloaded and installed redis-server, and  downloaded and installed the redis library for Python.
3.2. Use existing access log to count % of successful requests per hour.
The result of this task is in access_log.txt 
Also if you want to ensure that it works you should change in this file -> /home/o.schurenko/Documents/datamining.log , on your way where exists datamining.log  in your computer.
You should copy and run this code (3.2. OUT OF THE BOX) from access_log.txt in unix command line.