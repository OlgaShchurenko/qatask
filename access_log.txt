2.2 NICE TO HAVE:
Use existing access log to receive next information: Count # of successful requests per hour.
In this task I worked with  awk command.
Use unix command line (to run it in your computer change /home/o.schurenko/Documents/datamining.log on your way were exists datamining.log) :

cat /home/o.schurenko/Documents/datamining.log | awk '{if ($9==200) split($4,array,":") hour[array[2]]++ total++}END{for(element in hour)print element,hour[element]}'| sort
The result:
12 2
13 82
14 56
15 245
16 372
17 78

3.2 OUT OF THE BOX:
Use existing access log  to receive next information: Count % of successful requests per hour.
In this task I worked with  awk command.
Use unix command line (to run it in your computer change /home/o.schurenko/Documents/datamining.log on your way were exists datamining.log) :

cat /home/o.schurenko/Documents/datamining.log | awk '{if ($9==200) split($4,array,":") hour[array[2]]++ total++}END{for(element in hour)printf "%d %d/%d %.2f%\n",element,hour[element], total, hour[element]/total*100}'| sort
The result:
12 2/835 0.24%
13 82/835 9.82%
14 56/835 6.71%
15 245/835 29.34%
16 372/835 44.55%
17 78/835 9.34%

