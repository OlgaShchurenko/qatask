#For NICE TO HAVE level I had to verify that all methods working as expected, and all data is correctly added to server through positive and negative verifications.

#On this tests I used json and requests libraries.
import json # add this library only when you start work with this tests
import requests # add this library only when you start work with this tests
# Sometimes this method will use as PreConditions
# Test.1 test will verify method GET, get the list of all candidates. Response 200 -->Positive
class RequestGet: # GET method (Response 200), I want to see all candidates:
	def getmethodurl(self):
		self.url= 'http://qainterview.cogniance.com/candidates'
		headers = {'content-type': 'application/json'}
		self.our_request = requests.get(self.url, headers=headers)

x = RequestGet()
x.getmethodurl()
x.our_request.status_code # check status code 200
#print x.our_request.content # the list of candidates
if(x.our_request.status_code==200):
    print 'Test.1 Successful.Method GET gives the list of all candidates - Response 200'
else:
    print 'Test.1 Failed. Method GET did not give the list of all candidates - Response is wrong:' + str(x.our_request.status_code)
#___________________________________________________________________________________________________
#Test.2 Add a new candidate on server which accepts name and position,headers = {'content-type': 'application/json'},Response -201 -> positive test
#STEPS to do: 
#1. Get the list of all candidates (PreCondition) use previous Test.1
#2. Add candidates which accepts name and position
#3. Verify that a new candidate added on server

#1.PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost:  #2. Add candidates which accepts name and position
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod("Test2_name", "Test2_position") # I can create any name and position on server
if(x.our_request.status_code==201):
    #print 'Test.2 Successful. A new candidate was added - Response 201'
    #I need to check my new added data on server that is why I used r.json method to parse data on server into Python objects.
    #url= 'http://qainterview.cogniance.com/candidates'
    #headers = {'content-type': 'application/json'}
    #r = requests.get(url, headers=headers)
    #commit_data = r.json()
    #print(commit_data.keys())
    #[u'candidates']
    #print(commit_data [u'candidates'])
#   The result of my parsing is:
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']

    #It helped me to create Class that can easily find my added data on server by id using GET method:
    class getData: #3. Verify that a new candidate added on server
        def getDataID(self, id):
            url= 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url)
            commit_data = self.our_respond.json()
            for element in commit_data[u'candidates']:
                if element[u'id'] == id:
                    return element
    x = getData()
    response_get = x.getDataID(response_id) # id which I created before by POST method
    if(add_post_json==response_get):
        print 'Test.2 Successful. A new candidate was created and verified'
    else:
        print 'Test.2 Failed. A new candidate was not created'
else:
    print 'Test.2 Failed. Add a new candidate Response: ' + str(x.our_request.status_code)


#______________________________________________________________________________________________________
#Test.3 Next test add a new candidate which accepts only position,Content-Type: application/json Response -400--->Positive
#Steps to do:
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate without name but have position
#3. Verify status code 400

#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add a candidate without name but have position
	def postmethodEmptyName(self, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethodEmptyName('qqqqqq')
#print x.our_request.json()
if(x.our_request.status_code==400): #3. Verify status code
    print 'Test.3 Successful. A new candidate that accepts position and headers was not added on server. Response: 400'
else:
    print 'Test.3 Failed. A new candidate that accepts position and headers was added on server. Response: ' + str(x.our_request.status_code)
#method couldn't find data on server
#___________________________________________________________________________________________________
#Test.4  Next test add a new candidate which accepts name and position in body without headers = {'content-type': 'application/json'} Response -400--->Positive
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate with name and position but without headers = {'content-type': 'application/json'}
#3. Verify response 400
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add candidate with name and position but without headers = {'content-type': 'application/json'}
	def postmetodemptyheader (self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		self.our_request = requests.post(url,self.data)
x = RequestAddPost()
x.postmetodemptyheader('www', 'www')
x.our_request.status_code
if(x.our_request.status_code==400): #3. Verify response 400
    print 'Test.4 Successful. A new candidate was not added without headers. Response 400 '
else:
    print 'Test.4 Failed. A new candidate was added without headers. Response: ' + str(x.our_request.status_code)

#______________________________________________________________________________________________
#Test.5  Add similar candidates which accepts name and position, Content-Type: application/json --->Negetive
#Steps to do: 
#1. Get all candidates(PreCondition) use Test.1
#2. Add similar candidates with name and position,  headers = {'content-type': 'application/json'}
#3. Verify that candidates added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add similar candidates
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod('Test.5','QA')
first_post_status_code = x.our_request.status_code
y = RequestAddPost()
y.postmethod('Test.5','QA') #add the same data
second_post_status_code = y.our_request.status_code

if(first_post_status_code==201 and second_post_status_code==201):#3. Verify that candidates added on server
    add_post_json1 = x.our_request.json()[u'candidate']
    response_id1 = add_post_json1[u'id']

    add_post_json2 = y.our_request.json()[u'candidate']
    response_id2 = add_post_json2[u'id']

    if(response_id1!=response_id2):
        print "Test.5 Two similar candidates were added on server"
    else:
        print "Test.5 Two similar candidates can not be added on server"
else:
    print "Test.5 Two similar candidates can not be added on server, because first or second post status code not equil 201"
#______________________________________________________________________________________________
#Test.6  Add a new candidate which accepts only name in body,Content-Type: application/json --->Negative
##Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate with name and headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add candidate with name and headers
	def postmetodemptyPosition (self, name):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data,headers)
x = RequestAddPost()
x.postmetodemptyPosition('wwww')
x.our_request.status_code

if(x.our_request.status_code==201):  #3. Verify that a new candidate added on server
    print 'Test.6 A new candidate was added with name and headers - Response 201'

    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class getData:
            def getDataID(self, id):
                url= 'http://qainterview.cogniance.com/candidates'
                self.our_respond = requests.get(url)
                commit_data = self.our_respond.json()
                for element in commit_data[u'candidates']:
                    if element[u'id'] == id:
                        return element
    x = getData()
    response_get = x.getDataID(response_id)
    if(add_post_json==response_get):
        print 'Test.6 A new candidate was added with name and headers and verified by get method by id'
    else:
        print 'Test.6 A new candidate was not added with name and headers, verified by get method by id' # Check my added data on server
else:
    print 'Test.6 A new candidate can not be added without position. Response: ' + str(x.our_request.status_code)
#________________________________________________________________________________________________________
#Test.7 Add a new candidate which doesn't accept name and position, Content-Type: application/json --->Negative
##Steps to do: 
#1. Get all candidates, find what id is the last (PreCondition) use Test.1
#2. Add candidate without name and position but with headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl, find the last id
class RequestAddPost:#2. Add candidate without name and position
	def postmetodemptydata (self):
		url= 'http://qainterview.cogniance.com/candidates'
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url, headers=headers)

x = RequestAddPost()
x.postmetodemptydata()
x.our_request.status_code
if(x.our_request.status_code==201): #3. Verify that a new candidate added on server
    print 'Test.7 A new candidate was added without name and position - Response 201'

    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class getData:
            def getDataID(self, id):
                url= 'http://qainterview.cogniance.com/candidates'
                self.our_respond = requests.get(url)
                commit_data = self.our_respond.json()
                for element in commit_data[u'candidates']:
                    if element[u'id'] == id:
                        return element
    x = getData()
    response_get = x.getDataID(response_id)
    if(add_post_json==response_get):
        print 'Test.7 A new candidate was added without name and position and verified by get method by id'
    else:
        print 'Test.7 A new candidate was not added without name and position, verified by get method by id' # Check my added data on server
else:
    print 'Test.7 A new candidate was not added without name and position. Response: ' + str(x.our_request.status_code)

#___________________________________________________________________________________________________
#Test.8 Next test will verify can I create two names, headers = Content-Type: application/json -->Negative
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add a candidate with two names, headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add a candidate with two names
	def postmethodchangeposition(self, name, name2):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'name': name2})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
name1 = 'pop'
name2 = 'top'
x.postmethodchangeposition(name1,name2)
x.our_request.status_code
#201

if(x.our_request.status_code==201): #3. Verify that a new candidate added on server
    #print 'Test.8 A new candidate was added with two names - Respond 201'

    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']
    response_name = x.our_request.json()[u'candidate'][u'name']
    response_position = x.our_request.json()[u'candidate'][u'position']

    if(response_name==name1):
        print 'Test.8 A new candidate was added with two names, name = name1' # I check were is the name ('pop')
    elif(response_name==name2):
        print 'Test.8 A new candidate was added with two names, name = name2' # I check were is the name ('top')
else:
    print 'Test.8 Two names can not be created. Response: ' + str(x.our_request.status_code)
#______________________________________________________________________________________________
#Test.9 Next test will verify can I add a new candidate with two position in body, headers = Content-Type: application/json--> Negative
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate with two positions in body, headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add candidate with two positions in body
	def postmethodchangename(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'position': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
position1 = 'pop'
position2 = 'top'
x.postmethodchangename(position1,position2)
x.our_request.status_code
#201

if(x.our_request.status_code==201): #3. Verify that a new candidate added on server
    print 'Test.9 A new candidate with two positions can be created - Respond 201'

    add_post_json = x.our_request.json()[u'candidate']
    response_position = x.our_request.json()[u'candidate'][u'position']

    if(response_position==position1):
        print 'Test.9 A new candidate was added with two positions, position = position1' #I check were is the position ('pop')
    elif(response_position==position2):
        print 'Test.9 A new candidate was added with two positions, position = position2' #I check were is the position ('top')
else:
    print 'Test.9  A new candidate with two positions can not be created. Response: ' + str(x.our_request.status_code)
#______________________________________________________________________________________________
#Test.10 Add a new candidate which accepts empty name and empty position in body,Content-Type: application/json. --->Negative
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate with empty name 'None' and empty position 'None' in body, headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add candidate with empty name 'None' and empty position 'None'
    def postmethod(self, name, position):
        url = 'http://qainterview.cogniance.com/candidates'
        self.data = json.dumps({'name': None, 'position': None})
        headers = {'content-type': 'application/json'}
        self.our_request = requests.post(url, self.data, headers=headers)


x = RequestAddPost()
x.postmethod(None, None)
x.our_request.status_code
if (x.our_request.status_code == 201):#3. Verify that a new candidate added on server
    #print 'Test.10 A new candidate was added with null name and position - Respond 201'
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class getData:
        def getDataID(self, id):
            url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url)
            commit_data = self.our_respond.json()
            for element in commit_data[u'candidates']:
                if element[u'id'] == id:
                    return element

    x = getData()
    response_get = x.getDataID(response_id)  #  the name which I created before by POST method
    if (add_post_json == response_get):
        print 'Test.10 A new candidate was added with null name and position'
    else:
        print 'Test.10 A new candidate can not be added with null name and position'  # Check my added data on server

else:
    print 'Test.10 Can not created a new candidate with null name and position. Response: ' + str(x.our_request.status_code)
#___________________________________________________________________________________________________
#Test.11 Add a new candidate which accepts 40 symbols in name and position in body,Content-Type: application/json, -->Negative
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add candidate with 40 symbols in name and position in body, headers = {'content-type': 'application/json'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add candidate with 40 symbols in name and position
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb')
x.our_request.status_code

if (x.our_request.status_code == 201): #3. Verify that a new candidate added on server
    #print 'Test.11 A new candidate is created and accepts 40 symbols in name and position - Respond 201'
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class getData:
        def getDataID(self, id):
            url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url)
            commit_data = self.our_respond.json()
            for element in commit_data[u'candidates']:
                if element[u'id'] == id:
                    return element

    x = getData()
    response_get = x.getDataID(response_id)   #  the name which I created before by POST method
    if (add_post_json == response_get):
        print 'Test.11 A new candidate was created and accepts 40 symbols in name and position'
    else:
        print 'Test.11 A new candidate can not be created and accepts 40 symbols in name and position'  # Check my added data on server

else:
    print 'Test.11 A new candidate was not created because accepts 40 symbols in name and position. Response: ' + str(x.our_request.status_code)
#______________________________________________________________________________________________________
#Test.12  Add a new candidate which has wrong Content-Type:'application/x-www-form-urlencoded' -->Negative
#Steps to do: 
#1. Get all candidates (PreCondition) use Test.1
#2. Add a new candidate with name and position in body,but wrong headers = {'content-type': 'application/x-www-form-urlencoded'}
#3. Verify that a new candidate added on server
#1. PreCondition GET--> class RequestGet:def getmethodurl
class RequestAddPost: #2. Add a new candidate which has wrong Content-Type:'application/x-www-form-urlencoded'
	def postmethodwrongheader(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/x-www-form-urlencoded'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethodwrongheader('sos','sos')
x.our_request.status_code

if (x.our_request.status_code == 201): #3. Verify that a new candidate added on server
    #print 'Test.12 A new candidate that has wrong Content-Type was added on server - Respond 201'
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class getData:
        def getDataID(self, id):
            url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url)
            commit_data = self.our_respond.json()
            for element in commit_data[u'candidates']:
                if element[u'id'] == id:
                    return element

    x = getData()
    response_get = x.getDataID(response_id)  # put the name which I created before by POST method
    if (add_post_json == response_get):
        print 'Test.12 A new candidate that has wrong Content-Type was added on server and verified by get method by id'
    else:
        print 'Test.12 A new candidate that has wrong Content-Type was not added on server, verified by get method by id'

else:
    print 'Test.12 A new candidate  that has wrong Content-Type was not added on server. Response:' + str(x.our_request.status_code)
#data is not added
#______________________________________________________________________________________________________
#Test.13 Verify candidates on server by id:  Response - 200 ---->  Positive
#Steps to do:
#1. Add a new candidate
#2. Add again a new candidate
#3. Verify the id of this candidates
# When I was testing method for OK_LEVEL, I have noticed that method GET that search by id always shows the same result, this method works not correct.
# So if I add two id on server I must get two different id, and their json also will be different. So I will try to check it:

class RequestAddPost: #1. Add a new candidate
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost() #1. Add a new candidate
x.postmethod('zoo','QA')

y = RequestAddPost()
y.postmethod('cc','QA') #2. Add again a new candidate

response_id_x = x.our_request.json()[u'candidate'][u'id']
response_id_y = y.our_request.json()[u'candidate'][u'id']

class getData: #3. Verify the id of this candidates
        def getDataID(self, id):
            url= 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url + "/" + str(id))
            return self.our_respond.json()

get_x = getData()
get_y = getData()
get_x_json = get_x.getDataID(response_id_x)
get_y_json = get_y.getDataID(response_id_y)
print get_x_json
print get_y_json

if(get_x_json==get_y_json): # check id , have they their own id
    print 'Test.13 Failed. Method GET by id shows that two candidates have equal id. Response:' + str(x.our_request.status_code) #json is equil
else:
    print 'Test.13 Successful. The candidates on server verified  by id. Response - 200'

#______________________________________________________________________________________________________
#Test.14 Verify a new candidate by name and position in body ('qwertyuiop','qwertyuiop')---->Negative
#Steps to do:
#1. Create a new candidate with name 'qwertyuiop' and position 'qwertyuiop'
#2. Verify the new candidate which was created before by name and position
class RequestAddPost:
	def postmethod(self, name, position): #1. Create a new candidate
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
name1 = 'qwertyuiop'
position1 = 'qwertyuiop'
x.postmethod(name1,position1)
if(x.our_request.status_code==201): #2. Verify the new candidate which was created before by name and position
    #print 'Test.14 Successful. A new candidate was added - Response 201'
    class RequestGet:
        def getmethoddata (self,name,position):
            url= 'http://qainterview.cogniance.com/candidates'
            self.data = json.dumps({'name': name,  'position': position})
            self.our_request = requests.get(url,params=self.data)
    x = RequestGet()
    x.getmethoddata(name1,position1)
    x.our_request.json()
    if (x.our_request.status_code == 200):
        #print 'Test.14 Successful.- Respond 200'
        commit_data = x.our_request.json()
        # search for another name and position in our list of candidates
        test_failed = False
        for element in commit_data[u'candidates']:
            print element
            if (element[u'name'] != name1 and element[u'position'] != position1):
                test_failed = True
                break
        if(test_failed == True):
            print 'Test.14 Get method which verify data by name and position work incorrect'
        else:
            print 'Test.14 Get method which verify data by name and position work correct'

else:
    print 'Test.14 A new candidate was not added on server. Response: ' + str(x.our_request.status_code)
#___________________________________________________________________________________________________

#Test.15 Delete existing id: Response 201->Positive
#Steps to do:
#1.Create a new candidate
#2.Delete the new candidate by id
#3.Verify that this candidate was deleted

class RequestAddPost: #1.Create a new candidate
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod('sos','sos')

if (x.our_request.status_code == 201): #2.Delete the new candidate by id
    #print 'Test.15 Successful. A new candidate was created - Response 201'
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class deleteData:
        def deleteDataID(self, id):
            self.id = id
            self.url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond_delete = requests.delete(self.url + '/' + str(self.id))

    y = deleteData()
    y.deleteDataID(response_id)
    if (y.our_respond_delete.status_code==200):#3.Verify that this candidate was deleted
        print 'Test.15 Successful. The new candidate was deleted by id. Response - 200'
    else:
        print 'Test.15 Failed. The new candidate can not be deleted by id. Response^ ' + str(x.our_request.status_code)
else:
    print 'Test.15 Failed. The new candidate can not be created'
#___________________________________________________________________________________________________

#Test.16 Delete not existing id from server  --->Negative
#Steps to do:
#1.Create a new candidate
#2.Delete the new candidate by id
#3.Delete the new candidate by id again
#4.Verify that this candidate was deleted
class RequestAddPost:
	def postmethod(self, name, position):
		url= 'http://qainterview.cogniance.com/candidates'
		self.data = json.dumps({'name': name, 'position': position})
		headers = {'content-type': 'application/json'}
		self.our_request = requests.post(url,self.data, headers=headers)

x = RequestAddPost()
x.postmethod('sos','sos')
if (x.our_request.status_code == 201):
    #print 'Test.16 A new candidate was created - Response 201'
    add_post_json = x.our_request.json()[u'candidate']
    response_id = x.our_request.json()[u'candidate'][u'id']

    class deleteData:
        def deleteDataID(self, id):
            self.id = id
            self.url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond_delete = requests.delete(self.url + '/' + str(self.id))

    y = deleteData()
    y.deleteDataID(response_id) #2.Delete the new candidate by id
    z = deleteData()
    z.deleteDataID(response_id) #3.Delete the new candidate by id again
    if (y.our_respond_delete.status_code==200 and z.our_respond_delete.status_code ==200): #4.Verify that this candidate was deleted
    # if it is equal it will show that method works in wrong way
        print 'Test.16 Id which does not exist on server can be deleted'
    else:
        print 'Test.16 Id which does not exist on server can not be deleted'
else:
    print 'Test.16 A new candidate can not be created'

#___________________________________________________________________________________________________
#Test.17 Delete all candidates from server  --->Negative
#Steps to do:
#1.Get the list of all candidates
#2.Delete all candidates from server
#3.Verify that this all candidates were deleted
class getDataAndDelete:
        def getAllData(self):#1.Get the list of all candidates
            url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond = requests.get(url)
            commit_data = self.our_respond.json()
            return commit_data

        def deleteDataID(self, id):
            self.id = id
            self.url = 'http://qainterview.cogniance.com/candidates'
            self.our_respond_delete = requests.delete(self.url + '/' + str(self.id))

h = getDataAndDelete()
getAllData = h.getAllData()

for element in getAllData[u'candidates']:#2.Delete all candidates from server
    h.deleteDataID(element[u'id'])

if (h.our_respond_delete.status_code==200):#3.Verify that this all candidates were deleted
    print 'Test.17 All candidates were deleted from server'
else:
    print 'Test.17 All candidates can not be deleted from server'



		




	






				

 




